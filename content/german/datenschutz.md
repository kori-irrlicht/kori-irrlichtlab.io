---
title: Datenschutz & Haftung 
author: Kori
tags: [meta, datenschutz]
translationKey: "privacy"
---
# Datenschutz

Eine Erhebung von personenbezogenen Daten durch mich findet nicht statt. 

Allerdings wird diese Webseite von [GitLab Inc.](https://about.gitlab.com) ("GitLab") gehostet. GitLab erhebt Daten in Form von sogenannten "Server-Logfiles", die von Ihrem Browser automatisch an GitLab gesendet werden um diese Webseite darzustellen.

Laut GitLabs [Privacy Policy](https://about.gitlab.com/privacy/) (Stand: 08. Juni 2020 14:40 Uhr) werden dabei folgende Daten erhoben:
 -  Besuchte Website
 -  Uhrzeit zum Zeitpunkt des Zugriffes
 -  Menge der gesendeten Daten in Byte
 -  Quelle/Verweis, von welchem Sie auf die Seite gelangten
 -  Verwendeter Browser
 -  Verwendetes Betriebssystem
 -  Verwendete IP-Adresse
 
Eine genauere Erklärung über die erhobenen Daten und deren Verwendung sowie Kontaktdaten finden Sie in der [Privacy Policy](https://about.gitlab.com/privacy/) von GitLab.

# Haftung für Links
Auf dieser Website können Links zu Webseiten Dritter bestehen auf deren Inhalte ich keinen Einfluss habe und ich somit für diesen Inhalt nicht verantwortlich bin. Für die Inhalte der verlinkten Seiten sind alleine deren Anbieter und Betreiber verantwortlich.
Die verlinkten Seiten wurden vor der Verlinkung auf mögliche Rechtsverstöße geprüft. Zum Zeitpunkt der Verlinkung waren auf den verlinkten Seiten keinerlei Rechtsverstöße erkennbar.
Eine permanente inhaltliche Kontrolle der verlinkten Seiten ohne konkrete Anhaltspunkte einer Rechtsverletzung ist nicht zumutbar. Sollten zu einem späteren Zeitpunkt Rechtsverletzungen bekannt werden, werden
die entsprechenden Verlinkungen von der Webseite entfernt. 
