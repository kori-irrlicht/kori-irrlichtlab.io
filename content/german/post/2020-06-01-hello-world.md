---
author: Kori
title: "/dev/log 1: Hello world!"
tags: [meta, Project OpenMonster, promo-tool, promo-msg-gen]
date: 2020-06-01
---

# Hallo Welt!

In den letzten Tagen bin ich mit den grundlegenden Funktionen für die Tools ([Promo-Tool](https://gitlab.com/kori-irrlicht/promo-tool), [Message Generator](https://gitlab.com/kori-irrlicht/project-openmonster-message-generator)) fertig geworden, welche ich zur Weiterentwicklung von [Project OpenMonster](https://gitlab.com/kori-irrlicht/project-open-monster) benötige.

Da ich mich nun wieder auf das Hauptprojekt konzentrieren kann, dachte ich mir: "Das ist ein guter Punkt um einen devlog zu starten".

Beginnend mit diesem Post möchte ich in regulären Abständen Posts für die */dev/log* Reihe erstellen, in welcher ich über den Fortschritt in *Project OpenMonster* berichten werde.
Als Hobbyentwickler kann sich das manchmal schwierig gestalten, aber ich hoffe dennoch häufig Updates sowohl am */dev/log* als auch am Projekt selber vornehmen zu können.


# Was ist Project OpenMonster?
*Project OpenMonster* wird ein open-source Server für ein Monstersammelspiel. Während ich meinen Hauptfokus auf den Server selbst legen werde, werde ich nebenbei auch an einem grundlegendem 
Client mit der [Godot Engine](https://godotengine.org) arbeiten.
Dadurch, dass der Server Open Source ist und ich eine (hoffentlich) verständliche API erstelle, hoffe ich, dass die Community in der Lage ist eigene Clients zu programmieren.
Zu guter Letzt möchte ich noch erwähnen, dass das Projekt mit Hauptfokus auf Linux und Windows entwickelt wird.

Der Server wird zum großteil datenorientiert arbeiten. Dies ermöglicht mir und anderen neuen Content zu gestalten oder bereits existierenden Content wie Monster, Angriffe oder Items zu bearbeiten.
Aus meiner Sicht ist das ein wichtiger Punkt, da so das Spiel langfristig weiter leben kann.

So viel zum technischen. Wie bereits erwähnt wird *Project OpenMonster* ein Monstersammelspiel. Die Priorität wird darauf auf einem spaßigen Kampfsystem liegen. Derzeit ist ein rundenbasiertes System geplant, bei dem
beide Spieler jeweils zwei Monster gleichzeitig in den Kampf schicken werden. In späteren Versionen wird jeder Spieler eine eigene Dimension erhalten, in der sie Monster züchten und aufziehen können. 


# Was ist das Promo-Tool?
Das Promo-Tool (kurz für: Project OpenMonster) wird für die Erstellung von Content benutzt. Aktuell können folgende Sachen erstellt werden:
 - Monsterspezies
 - Items
 - Angriffe

Da ich zu faul bin jedes mal die JSON Struktur für jedes Monster, Item oder Angriff zu schreiben (was auch fehleranfällig sein kann), habe ich das Promo-Tool erstellt. Dieses bietet eine formularbasierte Erstellung des Contents
und erlaubt mir mich auf diesen zu konzentrieren.

# Was ist der Message Generator?
Der Message Generator nimmt die API Definitionen des Server als Eingabe entgegen und generiert das Model und Pfadkonstante für die Definition in der angegebenen Sprache (derzeit werden nur Go und Gdscript unterstützt).
Der Generator kann ebenfalls Validierungsfunktionen für das Model generieren, wodurch ich in der Definition die Validierungsfunktionen dokumentieren kann. Dadurch wird garantiert, dass Server und Client dieselben Validierungsfunktionen
besitzen, falls sie aus der API Definition generiert wurden.

