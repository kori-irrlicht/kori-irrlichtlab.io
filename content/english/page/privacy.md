---
title: Privacy & Liability 
author: Kori
tags: [meta, privacy]
translationKey: "privacy"
---
# Privacy Policy


I am not collecting any personally-identifying information.

The host of this website, [GitLab Inc.](https://about.gitlab.com) ("GitLab"), does collect and store data in so-called "server log files", which are automatically send by your webbrowser to render this website.
Referring to GitLabs [Privacy Policy](https://about.gitlab.com/privacy/) (Reviewed at: 08. Juni 2020 14:40 o'clock) the following data is collected:
 - Visited website
 - Time of the request
 - Amount of sent bytes
 - Referrer URL
 - Your browser
 - Your operating system
 - Your IP address
 
 Further information about the collected data and their usage and contact information are located in the [Privacy Policy](https://about.gitlab.com/privacy/) of GitLab.

# Liability for Links
This page might contain links to third-party websites on whose content I do not have influence and therefore I am not liable for that content. Only the provider or owner of the linked pages is always responsible for their content.
The linked websites were checked for legal violation at the time of linking. No illegal content was recognizable at the time of linking. A permanent control of the linked pages is not reasonable without concrete evidence of an infringement. Upon notification of violations, I will remove such links.
